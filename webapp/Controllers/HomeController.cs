﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using webapp.Models;

namespace webapp.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            throw new Exception("hello world");

            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            System.Threading.Thread.Sleep(3 * 60 * 1000);
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Help()
        {
            var th = new Thread(ExecuteInForeground);
            th.Start();
            Thread.Sleep(1000);
            ViewData["Message"] = "Your help page.";

            return View();
        }
        private static void ExecuteInForeground()
        {
            throw new Exception("exception from worker thread");
        }

            public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
