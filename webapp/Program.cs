﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace webapp
{
    public class Program
    {
        public static bool bCaptureStartErrors = false;
        public static void Main(string[] args)
        {
            bCaptureStartErrors = (string.Compare(System.Environment.GetEnvironmentVariable("MY_LOGS"), "hello", true) == 0);
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .CaptureStartupErrors(bCaptureStartErrors)
                .Build();
    }
}
